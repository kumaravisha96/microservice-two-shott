import React, { useState, useEffect } from 'react';

const HatForm = () => {
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");
    const [styleName, setStyleName] = useState("");
    const [picture, setPicture] = useState("");
    const [location, setLocation] = useState("");
    const [locations, setLocations] = useState([]);
  
  
    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);

        // Location dropdown list
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    // Handler function
    const handleFabric = (event) => {
        const value = event.target.value;
        setFabric(value)
    }
    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handleStyleName = (event) => {
        const value = event.target.value;
        setStyleName(value)
    }
    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value)
    }
    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value)
    }


    // Form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            fabric: fabric,
            color: color,
            style_name: styleName,
            picture: picture,
            location: location
        };

        // POST-ing to database
        const hatURL = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setFabric("");
            setColor("");
            setStyleName("");
            setPicture("");
            setLocation("");
        }
    };

    //return JSX
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create Hats!</h1>
                    <form onSubmit={handleSubmit} id="create-hat">

                        <div className="form-floating mb-3">
                            <input
                                name="style"
                                placeholder="StyleName"
                                required
                                type="text"
                                id="style"
                                className="form-control"
                                onChange={handleStyleName}
                                value={styleName}
                            />
                            <label htmlFor="color">Style Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                name="fabric"
                                placeholder="Fabric"
                                required
                                type="text"
                                id="fabric"
                                className="form-control"
                                onChange={handleFabric}
                                value={fabric}
                            />
                            <label htmlFor="fabric">Fabric</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                name="color"
                                placeholder="Color"
                                required
                                type="text"
                                id="color"
                                className="form-control"
                                onChange={handleColor}
                                value={color}
                            />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                name="picture"
                                placeholder="Picture"
                                required
                                type="text"
                                id="picture"
                                className="form-control"
                                onChange={handlePicture}
                                value={picture}
                            />
                            <label htmlFor="picture">Picture</label>
                        </div>

                        <div className="mb-3">
                            <select
                                required
                                name="location"
                                id="location"
                                className="form-select"
                                onChange={handleLocation}
                                value={location}
                            >

                                <option value="">Where is it?</option>
                                {locations.map((location) => {
                                return (
                                    <option
                                        key={location["href"]}
                                        value={location["href"]}
                                    >
                                        {location.closet_name}
                                    </option>
                                );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default HatForm