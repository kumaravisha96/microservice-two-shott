import { Link } from 'react-router-dom';
function ShoeList(props) {
    const deleteShoe = async (href) => {
        const url = `http://localhost:8080/${href}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }
    console.log(props.shoes)
    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Where is it</th>
                        <th></th>
                    </tr>
                </thead>
                 <tbody>
                    {props.shoes.map((shoes) => {
                        return (
                            <tr key={shoes.id}>
                                <td>{shoes.manufacturer}</td>
                                <td>{shoes.model_name}</td>
                                <td>{shoes.color}</td>
                                <td>
                                    <img src={shoes.picture_url} alt="" width="200px" height="200px" />
                                </td>
                                <td>{shoes.bin_number.closet_name}</td>
                                {<td>
                                    <button className="btn btn-outline-danger" onClick={() => deleteShoe(shoes.href)}>Delete</button>
                                </td>}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
              <Link to="/shoes/new" className="btn btn-warning btn-lg px-4 gap-3">Add New Shoe</Link>
            </div>
        </div>
    )
}

export default ShoesList;
