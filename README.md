# Wardrobify

Team:

* Avisha Kumar - Shoes
* Uch - Hats

## Design
Poll items from the Wardobe and Bin api to update our hat and shoe models. Those models will the return JSON objects for us to use in the frontend development.
We will use React to create components to show a user interface for to display the properties of the clothing items.
## Shoes microservice

The shoes microservice will consist of the shoe model and the bin value object. The retrieve information for our value object, we poll data about binsfrom the Wardrobe API. The BinVO will be used as the Foreign Key to our Shoe Models. We then use that connection to give the user options to select a bin when creating a shoe.



## Hats microservice

The Hats microservice provides data on the style, color, picture, and fabric material of the user collection of hats. This microservice allows the user to add hats to a database and provide the user with information on where to find said hats in their wardrobe. This wardrobe location data in polled from a seperate RESTful API. Since this data is collected (and polled) from a seperate service, location Value Objects (LocationVO) are created for all location database items. This provides one-sided communcation between the wardrobe API and the hats microservice where the hats microservices listens for updates to the location database for cannot directly change anything in said database.
