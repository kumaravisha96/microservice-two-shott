from django.db import models
from django.urls import reverse

# magic toy box that helps you keep track of your shoe collection
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()



class Shoe(models.Model):
    manufacturer = models.CharField(max_length=20)
    model_name = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(null=True)

    bin_number = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

# The first function, __str__(self), tells the toy box how to show the shoe's name when you ask for it.
# The second function, get_api_url(self), helps the toy box find the web address where you can see more details about the shoe.
    def __str__(self):
        return self.model_name
    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"id": self.id})
