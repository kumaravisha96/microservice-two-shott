from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.shortcuts import render
from .models import BinVO, Shoe


# Encoders: These tools help the toy computer show you the right information about your shoe bins and shoes.
class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "picture_url",
        "color",
    ]
    encoders = {
        "bin_number": BinVOListEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "picture_url",
        "color",
        "bin_number",
    ]
    encoders = {
        "bin_number": BinVOListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_Shoes(request, bin_vo_id=None):
    # show you all the shoes
    if request.method == "GET":
        if bin_vo_id == None:
            shoes = Shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    #  add a new shoe to your collection
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin_number"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin_number"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'DELETE'])
def api_show_shoe(request, id):
    # details of a specific shoe,
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            {"shoe": shoe}, encoder=ShoeDetailEncoder, safe=False
        )
    #  remove a shoe from your collection
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})


# shows you a list of all your shoes
@require_http_methods(["GET"])
def ShoeList(request):
    shoes = Shoe.objects.all()
    return JsonResponse(
        {"shoes": shoes}, encoder=ShoeDetailEncoder
    )
