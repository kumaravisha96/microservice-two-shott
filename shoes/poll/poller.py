import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            url = "http://wardrobe-api:8000/api/bins"
            response = requests.get(url)
            content = json.loads(response.content)

            for bin in content["bins"]:
                BinVO.objects.update_or_create(
                    import_href=bin["href"],
                    defaults={
                    "closet_name": bin["closet_name"],
                    "bin_number": bin["bin_number"],
                    "bin_size": bin["bin_size"],
                    }
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()


# Alright! Imagine you have a magic robot that helps you organize your shoes. This robot checks a website every minute to see if there are any updates about your shoe collection.
# Here's what the code does, step by step:
# The code tells the robot to keep checking forever by using while True:.

# Every time the robot checks, it says "Shoes poller polling for data" to let you know it's working.

# The robot tries to get information from a website using a special address (URL). It's like opening a webpage, but for the robot.

# If the robot can get the information successfully, it will read the list of your shoe bins (boxes) and the details about each one (like the closet name, bin number, and bin size).

# The robot then saves this information or updates it if it already has some details about the bins.

# If the robot has a problem getting the information or something goes wrong, it will tell you what happened.

# After checking the website, the robot takes a break for 60 seconds (1 minute) before checking again.

# Finally, the code starts the robot's shoe organizing task by calling the poll() function.